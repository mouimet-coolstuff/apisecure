import requests
from flask import abort, request
import os

def read_token(token):
    '''
    Read token from Keycloak and return informations from Keycloak (username, roles, valid, etc)

    :param token: Token to be read
    :return: Dictionary containing token informations. Raise exception
    '''

    url = os.environ["SSO_URL"]
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    payload = {
        "token": token,
        "client_id": os.environ["SSO_CLIENT_ID"],
        "client_secret": os.environ["SSO_SECRET"]
    }

    try:
        print("Read token " + token)
        print("Keycloak URL" + url)

        response = requests.post(url, data=payload, verify="ca-chain.cert.pem")

    except Exception as e:
        print("Error while retriving token informations. %s" % e.message)

    print("Keycloak token read status code: %s" % response.status_code)
    if response.status_code == 200:
        print("Token infos %s" % response.json())

        return response.json()
    else:
        return None


def isTokenValid(token):
    '''
    Validate to Keycloak if the token passed in paramater is valid.

    :param token: Token to be validated
    :return: True if token is valid, False if not.
    '''

    token_infos = read_token(token)

    if token_infos:
        return token_infos["active"]

    return False


def keycloak_secure(fn):
    """Decorator that checks that requests
    contain an id-token in the request header.
    userid will be None if the
    authentication failed, and have an id otherwise.

    Usage:
    @app.route("/")
    @authorized
    def secured_root(userid=None):
        pass
    """

    def _wrap(*args, **kwargs):
        if 'Authorization' not in request.headers:
            # Unauthorized
            print("No token in header")
            abort(401)
            return None

        print("Checking token...")

        try:
            token_infos = read_token(request.headers['Authorization'])

        except Exception as e:
            # Got exception while reading token.
            # Unauthorized request.
            abort(401)

        if token_infos["active"]:
            return fn(*args, **kwargs)
        else:
            print("Token is not valid")
            abort(401)

    return _wrap
