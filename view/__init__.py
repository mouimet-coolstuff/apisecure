from flask_restplus import Api
from view.secured_view import api as secured_api
from view.kc_view import api as kc_api

api = Api(
    title='Secured API',
    version='0.1',
    description='Secured API',
    doc='/apidoc/'
)

api.add_namespace(secured_api, path='/api/')
api.add_namespace(kc_api, path='/api/')
