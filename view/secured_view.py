from flask import jsonify, request
from flask_restplus import Namespace, Resource
from service.secured_service import SecuredService
from view.api_security import keycloak_secure


api = Namespace('application', description='Application API')


@api.route("privatedata")
class SecuredView(Resource):

    def __init__(self, api):
        Resource.__init__(self, api)
        self._secured_service = SecuredService()

    @keycloak_secure
    def get(self):
        return {"private_message": self._secured_service.get_private_data()}
