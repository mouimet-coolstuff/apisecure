from flask import jsonify, request
from flask_restplus import Namespace, Resource


api = Namespace('application', description='Application API')


@api.route("kc")
class SecuredView(Resource):

    def __init__(self, api):
        Resource.__init__(self, api)

    def get(self):
        return {
            "realm": "lab",
            "auth-server-url": "https://keycloak.box.ictms.lab/auth",
            "ssl-required": "external",
            "resource": "frontend",
            "public-client": True,
            "confidential-port": 0
        }
